
# Price of an outcome
class Selection(object):

	# constructor with defaults
	def __init__(self, selection_id, name, num, den):
		self.selection_id  = selection_id
		self.name = name
		self.num = num
		self.den = den

	def __str__(self):
		return '\'{0}\' ({1}), Price: {2}/{3}'.format(self.name, self.selection_id, self.num, self.den)

# Storing an event, teams and odds
class Event(object):

	# constructor
	def __init__(self, name, time):
		self.name       = name
		self.time       = time

	def __str__(self):
		return '\'{0}\' @ {1}'.format(self.name, str(self.time))

