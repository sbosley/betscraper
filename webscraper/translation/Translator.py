# simple class to control translating text into something useful

class Translator(object):

	def __init__(self,text,functor):
		self.functor = functor
		self.text = text

	def parse(self):
		return self.functor(self.text)


