# importing the datetime module
from datetime import datetime

# importing the project event model
from webscraper.model.EventModel import Selection

class BetVictorDateTimeTranslator(object):

	def __call__(self,dateTimeStr):
		# split "30 Mar 15:00 UK"
		day, month, time, timezone = dateTimeStr.replace('\n', '').split(' ')

		# split "15:00" into "15" and "00"
		hour, minute = time.split(':')

		return datetime(2013, datetime.strptime(month, "%b").month, int(day), int(hour), int(minute), 0)

class BetVictorSelectionTranslator(object):

	def __call__(self,selection):
		num, den = selection.split('/')
		return num, den

class BetVictorSelectionNameTranslator(object):

	def __call__(self,selection_num):
		if selection_num == 0:
			leg_name = "Home"
		elif selection_num == 1:
			leg_name = "Draw"
		elif selection_num == 2:
			leg_name = "Away"
		else:
			raise "No Selection found"

		return leg_name
