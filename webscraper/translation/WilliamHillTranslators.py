# importing the datetime module
from datetime import datetime

# importing the project event model
from model.EventModel import Selection

class WilliamHillDateTimeTranslator(object):

	def __call__(self,dateTimeStr):
		# split "30 Mar 15:00 UK"
		day, month, time, timezone = dateTimeStr.replace('\n', '').split(' ')

		# split "15:00" into "15" and "00"
		hour, minute = time.split(':')

		return datetime(2013, datetime.strptime(month, "%b").month, int(day), int(hour), int(minute), 0)

class WilliamHillSelectionTranslator(object):

	def __call__(self,selection):
		# are we evens?
		if selection == "EVS":
			num = 1
			den = 1
		else:
			num, den = selection.split('/')

		return num, den

class WilliamHillSelectionNameTranslator(object):

	def __call__(self,selection_num):
		if selection_num == 0:
			leg_name = "Home"
		elif selection_num == 1:
			leg_name = "Draw"
		elif selection_num == 2:
			leg_name = "Away"
		else:
			raise "No Selection found"

		return leg_name
