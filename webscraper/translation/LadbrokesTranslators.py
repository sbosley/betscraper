# importing the datetime module
from datetime import datetime

# importing the project event model
from model.EventModel import Selection

class LadbrokesDateTimeTranslator(object):

	def __call__(self,dateTimeStr):
		# split "30\xa0Mar.12:45" into "30" and "Mar.12:45"
		day, rest = dateTimeStr.split(u'\xa0')
		
		# split "Mar.12:45" into "Mar" and "12:45"
		month, time = rest.split('.')

		# split "12:45" into "12" and "45"
		hour, minute = time.split(':')
		
		return datetime(2013, datetime.strptime(month, "%b").month, int(day), int(hour), int(minute), 0)

class LadbrokesSelectionTranslator(object):

	def __call__(self,selection):
		# are we evens?
		if selection == "evens":
			num = 1
			den = 1
		else:
			num, den = selection.split('/')

		return num, den

class LadbrokesSelectionNameTranslator(object):

	def __call__(self,selection_num):
		if selection_num == 0:
			leg_name = "Home"
		elif selection_num == 1:
			leg_name = "Draw"
		elif selection_num == 2:
			leg_name = "Away"
		else:
			raise "No Selection found"

		return leg_name
