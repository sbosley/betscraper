# import standard modules
import string
from datetime import datetime

# import installed modules
from bs4 import BeautifulSoup

# import event model modules
from model.EventModel import Selection
from model.EventModel import Event

# import ladbrokes parsing functors
from translation.Translator import Translator
from parsers.WDWTableParser import WDWTableParser

class LbrWDWTableParser(WDWTableParser):

	def getEventRows(self, table):
		# get each row in the table excluding the first (header)
		rows = table.find_all("tr")
		rows.pop(0)
		return rows

	def getEventFromRow(self, row):
		# get the event name
		a_ev       = row.find("a", "eventLink")
		event_text = a_ev.get_text()
		
		# split the event description into team names
		teams     = event_text.split(' v ')
		home_team = self.display_name_map.get(teams[0], teams[0])
		away_team = self.display_name_map.get(teams[1], teams[1])

		event_name = '{0} v {1}'.format(home_team, away_team)

		# get the event time
		div_ev_time     = row.find("div", "eventTime")
		event_time_text = div_ev_time.get_text()
		event_time      = Translator(event_time_text, self.datetime_translator).parse()	
			
		return Event(event_name, event_time)

	def getSelectionsFromRow(self, row):
		selections = []

		# get all of the selection ids and odds for this row
		td_odds = row.find_all("td", "odds")
		for o in range(0,len(td_odds)):
			selections.append(self.getSelection(td_odds[o], Translator(o, self.selection_name_translator).parse()))

		return selections

	def getSelection(self, td_odd, name):
		# get the a link tag
		a_element = td_odd.find("a") 
		
		# get the selection ids
		a_id = a_element['id']
		ev_oc_id = int(a_id[2:])

		# get the selection text
		price_fraction = a_element.get_text().splitlines()[1]
		num, den       = Translator(price_fraction, self.selection_translator).parse()
		
		return Selection(ev_oc_id, name, num,den)
