# this will loop thorugh every menu item and grab all markets
from parsers.PageParser import PageParser
from parsers.WhlWDWTableParser import WhlWDWTableParser
from translation.WilliamHillTranslators import WilliamHillSelectionTranslator
from translation.WilliamHillTranslators import WilliamHillDateTimeTranslator
from translation.WilliamHillTranslators import WilliamHillSelectionNameTranslator

class WilliamHillWebsiteParser(object):
	def __init__(self,display_name_map,element_name,class_name):
		wdw_urls              = self.getWinDrawWinPages()
		self.wdw_table_parser = self.getWinDrawWinParser(display_name_map)
		self.pages            = self.parseWinDrawWinPages(wdw_urls,element_name,class_name)


	def getWinDrawWinParser(self,display_name_map):
		# create the parser to go through the first table that is a Win-Draw_win market
		whlWDWTable = WhlWDWTableParser(	{	'selection':      WilliamHillSelectionTranslator(), 
												'datetime':       WilliamHillDateTimeTranslator(), 
												'selection_name': WilliamHillSelectionNameTranslator() 
											}, 
											display_name_map
										)
		return whlWDWTable

	def getWinDrawWinPages(self):
		# let's just hard-code this for now

		# 1. get win-draw-win markets
		urls = []

		urls.append('http://sports.williamhill.com/bet/en-gb/betting/t/295/English-Premier-League.html')
		urls.append('http://sports.williamhill.com/bet/en-gb/betting/t/292/English-Championship.html')
		urls.append('http://sports.williamhill.com/bet/en-gb/betting/t/129/English-FA-Cup.html')
		urls.append('http://sports.williamhill.com/bet/en-gb/betting/t/293/English-League-1.html')
		urls.append('http://sports.williamhill.com/bet/en-gb/betting/t/294/English-League-2.html')

		return urls

	def parseWinDrawWinPages(self,urls,element_name,class_name):

		pages = []
		for url in urls:
			pages.append(PageParser(url,element_name,class_name))

		return pages

	def getEventsAndSelections(self):

		# list of lists, selections and events
		events_list     = []
		selections_list = []
		
		for page in self.pages:
			events, selections = self.wdw_table_parser.parseTable(page.tables)
			events_list.append(events)
			selections_list.append(selections)

		return events_list, selections_list
