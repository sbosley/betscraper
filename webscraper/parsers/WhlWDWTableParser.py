# import standard modules
import string
from datetime import datetime

# import installed modules
from bs4 import BeautifulSoup

# import event model modules
from model.EventModel import Selection
from model.EventModel import Event

# import ladbrokes parsing functors
from translation.Translator import Translator
from parsers.WDWTableParser import WDWTableParser

class WhlWDWTableParser(WDWTableParser):

	def getEventRows(self, table):
		# get each row in the table that holds odds
		rows = table.find_all('tr', 'rowOdd')
		return rows

	def getEventFromRow(self, row):
		# get the event time
		tds = row.find_all('td')
		date_td = tds[0]
		time_td = tds[1]

		event_time_text = date_td.get_text() + ' ' + time_td.get_text()
		event_time      = Translator(event_time_text, self.datetime_translator).parse()	

		# get the event name
		event_name_td = tds[2]
		event_text    = event_name_td.get_text()

		# remove the html &nbsp; characters
		event_text = event_text.replace(u'\xa0','')
		
		# split the event description into team names
		teams     = event_text.split(' v ')
		home_team = teams[0].strip().replace('\n','')
		away_team = teams[1].strip().replace('\n','')

		# lookup official team names
		home_team = self.display_name_map.get(home_team,home_team)
		away_team = self.display_name_map.get(away_team,away_team)

		event_name = '{0} v {1}'.format(home_team, away_team)

		return Event(event_name, event_time)

	def getSelectionsFromRow(self, row):
		selections = []
		
		# get all of the selection ids and odds for this row
		tds = row.find_all('div', 'eventprice')
		for o in range(0,len(tds)):
			selections.append(self.getSelection(tds[o], Translator(o, self.selection_name_translator).parse()))		

		return selections

	def getSelection(self, td, name):
		# The selection price is in the following format:
		#
		# <div class="eventprice" id="tup_selection263405031price">
		#	
		#		21/10
		#	
		# </div>

		# strip the numeric ev_oc_id from string "tup_selection263405052price"
		td_id = td['id']
		td_id = td_id.replace('tup_selection', '').replace('price', '')
		ev_oc_id = int(td_id)

		# get the selection text
		price_fraction = td.get_text().replace('\n','')
		price_fraction = price_fraction.strip()
		num, den       = Translator(price_fraction, self.selection_translator).parse()
		
		return Selection(ev_oc_id, name, num, den)
