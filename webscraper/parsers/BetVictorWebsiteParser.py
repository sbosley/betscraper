# this will loop thorugh every menu item and grab all markets
from webscraper.parsers.PageParser import PageParser
from webscraper.parsers.BVWDWTableParser import BVWDWTableParser
from webscraper.translation.BetVictorTranslators import BetVictorSelectionTranslator
from webscraper.translation.BetVictorTranslators import BetVictorDateTimeTranslator
from webscraper.translation.BetVictorTranslators import BetVictorSelectionNameTranslator

class BetVictorWebsiteParser(object):
	def __init__(self, display_name_map, element_name, class_name):
		wdw_urls              = self.getWinDrawWinPages()
		self.wdw_table_parser = self.getWinDrawWinParser(display_name_map)
		self.pages            = self.parseWinDrawWinPages(wdw_urls,element_name,class_name)


	def getWinDrawWinParser(self,display_name_map):
		# create the parser to go through the first table that is a Win-Draw_win market
		bvWDWTable = BVWDWTableParser(		{	'selection':      BetVictorSelectionTranslator(), 
												'datetime':       BetVictorDateTimeTranslator(), 
												'selection_name': BetVictorSelectionNameTranslator() 
											}, 
											display_name_map
										)
		return bvWDWTable

	def getWinDrawWinPages(self):
		# let's just hard-code this for now

		# 1. get win-draw-win markets
		urls = []

		urls.append('http://www.betvictor.com/sports/en/football/coupons/100/6323610/0/1661/0/PE/0/0/0/0/1')
		#urls.append('http://www.betvictor.com/sports/en/football/coupons/100/6228010/0/1661/0/PE/0/0/0/0/1')
		#urls.append('http://www.betvictor.com/sports/en/football/coupons/100/6379210/0/1661/0/PE/0/0/0/0/1')
		#urls.append('http://www.betvictor.com/sports/en/football/coupons/100/6378810/0/1661/0/PE/0/0/0/0/1')
		#urls.append('http://www.betvictor.com/sports/en/football/eng-fa-cup/coupons/100/6228510/0/1661/0/MPE/0/0/0/12/1')
		
		return urls

	def parseWinDrawWinPages(self,urls,element_name,class_name):

		pages = []
		for url in urls:
			pages.append(PageParser(url,element_name,class_name))

		return pages

	def getEventsAndSelections(self):

		# list of lists, selections and events
		events_list     = []
		selections_list = []
		
		for page in self.pages:
			events, selections = self.wdw_table_parser.parseTable(page.tables)
			events_list.append(events)
			selections_list.append(selections)

		return events_list, selections_list
