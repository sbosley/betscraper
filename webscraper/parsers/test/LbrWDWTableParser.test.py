#!/usr/bin/python

# import system modules
import string
import sys
import urllib.request
from datetime import datetime

# import custom modules
from bs4 import BeautifulSoup

# import event model module
from model.EventModel import Selection
from model.EventModel import Event

# import ladbrokes specific module
from parsers.PageParser import PageParser
from parsers.LbrWDWTableParser import LbrWDWTableParser

# import database connection module
from db.Connection import Connection
from db.Connection import ConnectionFailedError
from db.DataAbstractionLayer import DataAbstractionLayer

from translation.LadbrokesTranslators import LadbrokesSelectionTranslator
from translation.LadbrokesTranslators import LadbrokesDateTimeTranslator
from translation.LadbrokesTranslators import LadbrokesSelectionNameTranslator

# get the premiership page from url
premierPage = PageParser('http://sports.ladbrokes.com/en-gb/Football/English/Premier-LeagueFootball/English/Premier-League-s84','table','tblOdds','LadbrokesPremierLeague.html')
#premierPage = PageParser('http://sports.ladbrokes.com/en-gb/Football/English/Premier-LeagueFootball/English/Premier-League-s84','tblOdds')

# connect to the database
params = dict()
params['host']   = "localhost"
params['user']   = "root"
params['passwd'] = ""
params['db']     = "betscraper"

# attempt db connection and record result
db  = Connection(params)
dal = DataAbstractionLayer(db)

# get a map of display names to real team names
bookmaker        = 'Ladbrokes'
display_name_map = dal.get_display_name_map('Football','Premier League',bookmaker)

# create the parser to go through the first table that is a Win-Draw_win market
lbrWDWTable = LbrWDWTableParser({	'selection':      LadbrokesSelectionTranslator(), 
									'datetime':       LadbrokesDateTimeTranslator(), 
									'selection_name': LadbrokesSelectionNameTranslator() 
								}, 
								display_name_map)

# parse...
events, selections = lbrWDWTable.parseTable(premierPage.tables)

# get the events and selections, insert into database
dal.insert_events(bookmaker, 'Win-Draw-Win', events, selections)
