#!/usr/bin/python

# import system modules
import string
import sys
import urllib.request
from datetime import datetime

# import event model module
from model.EventModel import Selection
from model.EventModel import Event

from parsers.WDWTableParser import WDWTableParser
from parsers.WilliamHillWebsiteParser import WilliamHillWebsiteParser

# import database connection module
from db.Connection import Connection
from db.Connection import ConnectionFailedError
from db.DataAbstractionLayer import DataAbstractionLayer

# get the premiership page from url

#premierPage = PageParser('http://sports.williamhill.com/bet/en-gb/betting/t/295/English-Premier-League.html','tableData')

# connect to the database
params = dict()
params['host']   = "localhost"
params['user']   = "root"
params['passwd'] = ""
params['db']     = "betscraper"

# attempt db connection and record result
db  = Connection(params)
dal = DataAbstractionLayer(db)

# get a map of display names to real team names
bookmaker = 'William Hill'
football_display_name_maps = dict()
football_display_name_maps['Premier League'] = dal.get_display_name_map('Football','Premier League',bookmaker)

# time to parse the william hill website
whlWebsiteParser = WilliamHillWebsiteParser(football_display_name_maps['Premier League'],'table','tableData')
events_list, selections_list = whlWebsiteParser.getEventsAndSelections()

for i in range(0,len(events_list)):
	dal.insert_events(bookmaker, 'Win-Draw-Win', events_list[i], selections_list[i])
