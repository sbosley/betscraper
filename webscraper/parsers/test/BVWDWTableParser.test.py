#!/usr/bin/python

# import system modules
import string
import sys
import urllib.request
from datetime import datetime

# import event model module
from webscraper.model.EventModel import Selection
from webscraper.model.EventModel import Event

from webscraper.parsers.WDWTableParser import WDWTableParser
from webscraper.parsers.BetVictorWebsiteParser import BetVictorWebsiteParser

# import database connection module
from db.Connection import Connection
from db.Connection import ConnectionFailedError
from db.DataAbstractionLayer import DataAbstractionLayer

# get the premiership page from url

#premierPage = PageParser('http://sports.williamhill.com/bet/en-gb/betting/t/295/English-Premier-League.html','tableData')

# connect to the database
params = dict()
params['host']   = "localhost"
params['user']   = "root"
params['passwd'] = ""
params['db']     = "betscraper"

# attempt db connection and record result
db  = Connection(params)
dal = DataAbstractionLayer(db)

# get a map of display names to real team names
football_display_name_maps = dict()
football_display_name_maps['Premier League'] = dal.get_display_name_map('Football','Premier League','Bet Victor')

# time to parse the bet victor website
bvWebsiteParser = BetVictorWebsiteParser(football_display_name_maps['Premier League'], 'div', 'bets_coupon three_way_outright_coupon')
events_list, selections_list = bvWebsiteParser.getEventsAndSelections()

for i in range(0,len(events_list)):
	print(WDWTableParser.printTable(events_list[i], selections_list[i]))



# get the events and selections, insert into database
#dal.insert_events(bookmaker, 'Win-Draw-Win', events, selections)

