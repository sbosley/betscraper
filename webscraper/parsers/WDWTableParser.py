# import standard modules
import string
from datetime import datetime

# import installed modules
from bs4 import BeautifulSoup

# import event model modules
from webscraper.model.EventModel import Selection
from webscraper.model.EventModel import Event

# import ladbrokes parsing functors
from webscraper.translation.Translator import Translator

import inspect

# Win-Draw-Win Table Parser
class WDWTableParser(object):
	# initialise translators and display name map
	def __init__(self, translators, display_name_map):
		self.selection_translator      = translators['selection']
		self.datetime_translator       = translators['datetime']
		self.selection_name_translator = translators['selection_name']
		self.display_name_map          = display_name_map

	# parse the html tables
	def parseTable(self,table):
		events     = []
		selections = []

		# each row should be one event
		rows = self.getEventRows(table)
		
		# row each row in the table, get the cells and splt each cell into items
		for row in rows:
			events.append(self.getEventFromRow(row))
			selections.extend(self.getSelectionsFromRow(row))

		return events, selections

	def getEventRows(self, table):
		raise NotImplementedError('Subclasses should implement {0} {1}'.format(__class__, inspect.stack()[0][3]))

	def getEventFromRow(self, row):
		raise NotImplementedError('Subclasses should implement {0} {1}'.format(__class__, inspect.stack()[0][3]))

	def getSelectionsFromRow(self, row):
		raise NotImplementedError('Subclasses should implement {0} {1}'.format(__class__, inspect.stack()[0][3]))

	def getSelection(self, td_odd, name):
		raise NotImplementedError('Subclasses should implement {0} {1}'.format(__class__, inspect.stack()[0][3]))

	@staticmethod
	def printTable(events,selections):
		output = '{0} Events...'.format(len(events))

		# loop events
		for e in range(0,len(events)):
			output += "\n" + str(events[e])

			# loop selections for this event
			for s in range(0,3):
				output += "\n" + str(selections[e*3+s])
		
		return output


