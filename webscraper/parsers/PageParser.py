# Take one page from the website and parse it for html tables containg markets, events, odds

# For downloading simple static web pages urllib will do the job
from urllib.request import urlopen
from urllib.request import Request

# To download non-static HTML we need selenium
from selenium import webdriver

# For parsing the HTML source we use beautiful soup
from bs4 import BeautifulSoup

# Page parser get's page and extracts the main content
class PageParser(object):

	def __init__(self,url,element_name,class_name,async=False,test_page=None):
		# normally download page, but if test page path supplied then get instead
		if test_page == None:
			html = self.download_url(url,async)
		else:
			html = open(test_page, 'r').read()

		self.tables = self.get_table(html,element_name,class_name)

	def download_url(self,url,async):
		# Synchronous webpages can be downloaded using simple request/response method
		# Otherwise we can use selenium to fully wait whilst all asynchronous webpage elements are loaded before retrieving web page source
		if async:
			browser = webdriver.Chrome()
			browser.get(url)
			content = browser.page_source
			browser.close()
		else:
			request = Request(url)
			response = urlopen(request)
			content = response.read()
		return content

	def get_table(self,html,element_name,class_name):
		root  = BeautifulSoup(html)
		table = root.find(element_name, {"class" : class_name})
		return table