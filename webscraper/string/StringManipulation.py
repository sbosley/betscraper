class StringManipulation():

	# Get all occurences of text appearing between start/end markers
	def getMatches(text, extractStart, extractEnd):
	
		# list of matches to return
		matches = []

		# Keep looping until we find our matching text or get to end
		while (text.find( extractStart ) != -1):

			# Extract the text starting with 'extractStart' and ending with 'extractEnd'
			before, match, after = text.partition( extractStart )
			before, match, after = after.partition( extractEnd )
			text = after
		
			# Add to our matches
			matches.append(before)

		return matches
