/*

This query is designed to get the spread for an event where you found some overround

*/

select 
	e.name as Event,

	b1.name as 'Bookmaker 1',
	b2.name as 'Bookmaker 2',

	CONCAT(h1.num, '/', h1.den) as 'Home 1',
	CONCAT(d1.num, '/', d1.den) as 'Draw 1',
	CONCAT(a2.num, '/', a2.den) as 'Away 2',

	(h1.den / (h1.num + h1.den) * 100) as 'Home 1',
	(d1.den / (d1.num + d1.den) * 100) as 'Draw 1',
	(a2.den / (a2.num + a2.den) * 100) as 'Away 2'

from 
	tselection h1, 
	tselection d1, 
	tselection a2,
	tevent e,
	tbookmaker b1, 
	tbookmaker b2 

where
	h1.name = 'Home' and
	d1.name = 'Draw' and
	a2.name = 'Away' and

	e.name = 'Southampton v Chelsea' and
	e.event_id = h1.event_id and
	h1.event_id = d1.event_id and
	d1.event_id = a2.event_id and

	h1.bookmaker_id = b1.bookmaker_id and 
	d1.bookmaker_id = b1.bookmaker_id and 
	a2.bookmaker_id = b2.bookmaker_id and 

	b1.name = 'Ladbrokes' and
	b2.name = 'William Hill';


/*
+-----------------------+-------------+-------------+----------+----------+----------+----------+----------+----------+
| Event                 | Bookmaker 1 | Bookmaker 2 | H1,D2,A2 | H2,D1,A2 | H2,D2,A1 | H2,D1,A1 | H1,D2,A1 | H1,D1,A2 |
+-----------------------+-------------+-------------+----------+----------+----------+----------+----------+----------+
| Southampton v Chelsea | Ladbrokes   | WilliamHill | 102.4242 | 103.5354 | 110.4618 | 110.4618 | 109.3506 |  99.8990 |
+-----------------------+-------------+-------------+----------+----------+----------+----------+----------+----------+


Result:

+-----------------------+-------------+-------------+--------+--------+--------+---------+---------+---------+
| Event                 | Bookmaker 1 | Bookmaker 2 | Home 1 | Draw 1 | Away 2 | Home 1  | Draw 1  | Away 2  |
+-----------------------+-------------+-------------+--------+--------+--------+---------+---------+---------+
| Southampton v Chelsea | Ladbrokes   | WilliamHill | 11/4   | 13/5   | 6/5    | 26.6667 | 27.7778 | 45.4545 |
+-----------------------+-------------+-------------+--------+--------+--------+---------+---------+---------+



Southampton v Chelsea

Ladbrokes
Place £26.67 @ 11/4 on Southampton
Place £27.78 @ 13/5 on a Draw

Stake = £54.45
Winnings = £73.3425 + £72.228 = £145.5705

William Hill
Place £45.45 @ 6/5 on Chelsea

Stake = £45.45
Winnings = £54.54


if it's a home win
------------------

winnings
£73.3425

losses
£27.78 + £45.45 = £73.23

profit = £73.3425 - £73.23 = £0.1125

if it's a draw
--------------

winnings
£72.228

losses
£26.67 + £45.45 = £72.12

profit = £72.228 - £72.12 = £0.108

if it's an away win
-------------------

winnings
£54.54

losses
£26.67 + £27.78 = £54.45

profit = £54.54 - 54.45 = £0.09

*/








