-- select the database we're going to put data into
use betscraper

-- create the bookmakers that we're running scripts for
insert into tBookmaker (name) values ('Ladbrokes');
insert into tBookmaker (name) values ('William Hill');
insert into tBookmaker (name) values ('Bet Victor');

-- insert the sport names
insert into tSport (name) values ('Football');

-- insert official competition names
insert into tCompetition (name,sport_id) value ('Premier League', (select sport_id from tSport where name = 'Football'));

-- insert the official premiership team names (alphabetical order)
insert into tCompetitor (name,competition_id) values ('Arsenal',              (select competition_id from tCompetition where name = 'Premier League'));
insert into tCompetitor (name,competition_id) values ('Aston Villa',          (select competition_id from tCompetition where name = 'Premier League'));
insert into tCompetitor (name,competition_id) values ('Chelsea',              (select competition_id from tCompetition where name = 'Premier League'));
insert into tCompetitor (name,competition_id) values ('Everton',              (select competition_id from tCompetition where name = 'Premier League'));
insert into tCompetitor (name,competition_id) values ('Fulham',               (select competition_id from tCompetition where name = 'Premier League'));
insert into tCompetitor (name,competition_id) values ('Liverpool',            (select competition_id from tCompetition where name = 'Premier League'));
insert into tCompetitor (name,competition_id) values ('Manchester United',    (select competition_id from tCompetition where name = 'Premier League'));
insert into tCompetitor (name,competition_id) values ('Manchester City',      (select competition_id from tCompetition where name = 'Premier League'));
insert into tCompetitor (name,competition_id) values ('Newcastle United',     (select competition_id from tCompetition where name = 'Premier League'));
insert into tCompetitor (name,competition_id) values ('Norwich City',         (select competition_id from tCompetition where name = 'Premier League'));
insert into tCompetitor (name,competition_id) values ('Reading',              (select competition_id from tCompetition where name = 'Premier League'));
insert into tCompetitor (name,competition_id) values ('Southampton',          (select competition_id from tCompetition where name = 'Premier League'));
insert into tCompetitor (name,competition_id) values ('Stoke City',           (select competition_id from tCompetition where name = 'Premier League'));
insert into tCompetitor (name,competition_id) values ('Sunderland',           (select competition_id from tCompetition where name = 'Premier League'));
insert into tCompetitor (name,competition_id) values ('Swansea City',         (select competition_id from tCompetition where name = 'Premier League'));
insert into tCompetitor (name,competition_id) values ('Tottenham Hotspur',    (select competition_id from tCompetition where name = 'Premier League'));
insert into tCompetitor (name,competition_id) values ('West Bromwich Albion', (select competition_id from tCompetition where name = 'Premier League'));
insert into tCompetitor (name,competition_id) values ('West Ham United',      (select competition_id from tCompetition where name = 'Premier League'));
insert into tCompetitor (name,competition_id) values ('Wigan Athletic',       (select competition_id from tCompetition where name = 'Premier League'));
insert into tCompetitor (name,competition_id) values ('Queens Park Rangers',  (select competition_id from tCompetition where name = 'Premier League'));

--
-- insert bet victor display names
--

insert into 
	tBookmakerDisplayName (name,bookmaker_id,competitor_id) 
values 
	(
		'West Brom', 
		(select bookmaker_id   from tBookmaker   where name = 'Bet Victor'),
		(select competitor_id  from tCompetitor  where name = 'West Bromwich Albion')
	);

insert into 
	tBookmakerDisplayName (name,bookmaker_id,competitor_id) 
values 
	(
		'West Ham', 
		(select bookmaker_id   from tBookmaker   where name = 'Bet Victor'),
		(select competitor_id  from tCompetitor  where name = 'West Ham United')
	);

--
-- insert ladbrokes display names
--
insert into 
	tBookmakerDisplayName (name,bookmaker_id,competitor_id) 
values 
	(
		'Manchester Utd', 
		(select bookmaker_id   from tBookmaker   where name = 'Ladbrokes'),
		(select competitor_id  from tCompetitor  where name = 'Manchester United')
	);


insert into 
	tBookmakerDisplayName (name,bookmaker_id,competitor_id) 
values 
	(
		'Man City', 
		(select bookmaker_id   from tBookmaker   where name = 'Ladbrokes'),
		(select competitor_id  from tCompetitor  where name = 'Manchester City')
	);

insert into 
	tBookmakerDisplayName (name,bookmaker_id,competitor_id) 
values 
	(
		'Tottenham', 
		(select bookmaker_id   from tBookmaker   where name = 'Ladbrokes'),
		(select competitor_id  from tCompetitor  where name = 'Tottenham Hotspur')
	);

insert into 
	tBookmakerDisplayName (name,bookmaker_id,competitor_id) 
values 
	(
		'West Brom', 
		(select bookmaker_id   from tBookmaker   where name = 'Ladbrokes'),
		(select competitor_id  from tCompetitor  where name = 'West Bromwich Albion')
	);


insert into 
	tBookmakerDisplayName (name,bookmaker_id,competitor_id) 
values 
	(
		'Swansea', 
		(select bookmaker_id   from tBookmaker   where name = 'Ladbrokes'),
		(select competitor_id  from tCompetitor  where name = 'Swansea City')
	);


insert into 
	tBookmakerDisplayName (name,bookmaker_id,competitor_id) 
values 
	(
		'Stoke', 
		(select bookmaker_id   from tBookmaker   where name = 'Ladbrokes'),
		(select competitor_id  from tCompetitor  where name = 'Stoke City')
	);


insert into 
	tBookmakerDisplayName (name,bookmaker_id,competitor_id) 
values 
	(
		'Norwich', 
		(select bookmaker_id   from tBookmaker   where name = 'Ladbrokes'),
		(select competitor_id  from tCompetitor  where name = 'Norwich City')
	);

insert into 
	tBookmakerDisplayName (name,bookmaker_id,competitor_id) 
values 
	(
		'Newcastle', 
		(select bookmaker_id   from tBookmaker   where name = 'Ladbrokes'),
		(select competitor_id  from tCompetitor  where name = 'Newcastle United')
	);

insert into 
	tBookmakerDisplayName (name,bookmaker_id,competitor_id) 
values 
	(
		'Wigan', 
		(select bookmaker_id   from tBookmaker   where name = 'Ladbrokes'),
		(select competitor_id  from tCompetitor  where name = 'Wigan Athletic')
	);

insert into 
	tBookmakerDisplayName (name,bookmaker_id,competitor_id) 
values 
	(
		'QPR', 
		(select bookmaker_id   from tBookmaker   where name = 'Ladbrokes'),
		(select competitor_id  from tCompetitor  where name = 'Queens Park Rangers')
	);

--
-- insert william hill display names
--

insert into 
	tBookmakerDisplayName (name,bookmaker_id,competitor_id) 
values 
	(
		'Man Utd', 
		(select bookmaker_id   from tBookmaker   where name = 'William Hill'),
		(select competitor_id  from tCompetitor  where name = 'Manchester United')
	);


insert into 
	tBookmakerDisplayName (name,bookmaker_id,competitor_id) 
values 
	(
		'Man City', 
		(select bookmaker_id   from tBookmaker   where name = 'William Hill'),
		(select competitor_id  from tCompetitor  where name = 'Manchester City')
	);

insert into 
	tBookmakerDisplayName (name,bookmaker_id,competitor_id) 
values 
	(
		'Tottenham', 
		(select bookmaker_id   from tBookmaker   where name = 'William Hill'),
		(select competitor_id  from tCompetitor  where name = 'Tottenham Hotspur')
	);

insert into 
	tBookmakerDisplayName (name,bookmaker_id,competitor_id) 
values 
	(
		'West Brom', 
		(select bookmaker_id   from tBookmaker   where name = 'William Hill'),
		(select competitor_id  from tCompetitor  where name = 'West Bromwich Albion')
	);


insert into 
	tBookmakerDisplayName (name,bookmaker_id,competitor_id) 
values 
	(
		'Swansea', 
		(select bookmaker_id   from tBookmaker   where name = 'William Hill'),
		(select competitor_id  from tCompetitor  where name = 'Swansea City')
	);


insert into 
	tBookmakerDisplayName (name,bookmaker_id,competitor_id) 
values 
	(
		'Stoke', 
		(select bookmaker_id   from tBookmaker   where name = 'William Hill'),
		(select competitor_id  from tCompetitor  where name = 'Stoke City')
	);


insert into 
	tBookmakerDisplayName (name,bookmaker_id,competitor_id) 
values 
	(
		'Norwich', 
		(select bookmaker_id   from tBookmaker   where name = 'William Hill'),
		(select competitor_id  from tCompetitor  where name = 'Norwich City')
	);

insert into 
	tBookmakerDisplayName (name,bookmaker_id,competitor_id) 
values 
	(
		'Newcastle', 
		(select bookmaker_id   from tBookmaker   where name = 'William Hill'),
		(select competitor_id  from tCompetitor  where name = 'Newcastle United')
	);

insert into 
	tBookmakerDisplayName (name,bookmaker_id,competitor_id) 
values 
	(
		'Wigan', 
		(select bookmaker_id   from tBookmaker   where name = 'William Hill'),
		(select competitor_id  from tCompetitor  where name = 'Wigan Athletic')
	);

insert into 
	tBookmakerDisplayName (name,bookmaker_id,competitor_id) 
values 
	(
		'QPR', 
		(select bookmaker_id   from tBookmaker   where name = 'William Hill'),
		(select competitor_id  from tCompetitor  where name = 'Queens Park Rangers')
	);









