-- drop it if it already exists, then create it
drop database if exists betscraper;
create database betscraper;

-- say that we're going ro use this database
use betscraper

create table tSport
(
	sport_id int not null auto_increment,
	name varchar(20) not null,
	primary key (sport_id),
	unique index (name)
);

create table tCompetition
(
	competition_id int not null auto_increment,
	sport_id int not null,
	name varchar(20) not null,
	primary key (competition_id),
	foreign key (sport_id) references tSport(sport_id),
	unique index (name)
);

create table tCompetitor
(
	competitor_id int not null auto_increment,
	competition_id int not null,
	name varchar(20) not null,
	primary key (competitor_id),
	foreign key (competition_id) references tCompetition(competition_id),
	unique index (name)
);

create table tBookmaker
(
	bookmaker_id int not null auto_increment,
	name varchar(20) not null,
	primary key (bookmaker_id),
	unique index (name)
);

create table tBookmakerDisplayName
(
	bookmaker_display_name_id int not null auto_increment,
	bookmaker_id int not null,
	competitor_id int not null,
	name varchar(20) not null,
	primary key (bookmaker_display_name_id),
	foreign key (bookmaker_id) references tBookmaker(bookmaker_id),
	foreign key (competitor_id) references tCompetitor(competitor_id),
	unique index (name,bookmaker_id)
);

-- event
--
-- event_id - pk
-- name  - name of the event
-- time  - time of the event

create table tEvent
(
	event_id int not null auto_increment,
	name varchar(50) not null,
	time datetime not null,
	primary key (event_id),
	unique index (name, time)
);

-- event market
--
-- market_id - pk
-- name      - name of the market, example Win-Draw-Win

create table tMarket
(
	market_id int not null auto_increment,
	name varchar(30) not null,
	primary key (market_id),
	unique index (name)
);

-- selection
-- 
-- selection_id  - pk bookmakers selection id, only unique with bookmaker_id
-- bookmaker_id  - bookmaker displaying these odds
-- market_id     - fk market
-- event_id      - fk to tevent
-- name          - name of the selection
-- num           - price numerator
-- den           - price denominator

create table tSelection
(
	selection_id int not null,
	bookmaker_id int not null,
	event_id     int not null,
	market_id    int not null,
	name         varchar(20) not null,
	num          int not null,
	den          int not null,
	primary key (selection_id, bookmaker_id),
	foreign key (bookmaker_id) references tBookmaker(bookmaker_id),
	foreign key (market_id) references tMarket(market_id)
);

-- tPriceUpdate
-- price update notification, for each time we find a new price, or the latest price changes
-- 
-- price_update_id   - pk this is like an id in the queue of notifications
-- price_update_type - either 'I' for price insert or 'U' for a price update
-- selection_id      - fk composite with bookmaker_id to refer to selection that notification is about
-- bookmaker_id      - fk composite as above

create table tPriceUpdate
(
	price_update_id int not null auto_increment,
	price_update_type char(1) not null,
	`timestamp` timestamp not null default now(),
	selection_id int not null,
	bookmaker_id int not null,
	old_num int null,
	old_den int null,
	new_num int null,
	new_den int null,
	primary key (price_update_id),
	foreign key (selection_id, bookmaker_id) references tSelection(selection_id, bookmaker_id)
);

/*
create table tBookmakerEvent
(
	bookmaker_event_id int not null auto_increment,
	primary key (bookmaker_event_id)
);


create table tBookmakerMarket
(
	bookmaker_market_id int not null auto_increment,
	bookmaker_event_id int not null,
	primary key (bookmaker_market_id)
);

create table tBookmakerSelection
(
	bookmaker_selection_id int not null auto_increment,
	primary key (bookmaker_selection_id)
);




create table tSport
(
	sport_id int not null auto_increment,
	desc varchar(100),
	primary key(sport_id)
);

create table tTeam
(
	team_id int not null auto,
	desc varchar(100),
	primary key(team_id)
);
*/