/*

This query is designed to get the overround when mixing the outcomes between two bookmakers

This query compares each selection from one bookmaker (home/draw/away) against the other two selections from another bookmaker.

If bookmaker one is the home selection then it will pick draw and away from bookmaker two. It then calculates the overround of

all three selections. It filter's out overrounds greater than 100 where there is no arbitrage to be made.

*/

select
	e.name as Event,
	e.time as Time,
	b1.name as Bookmaker,
	s1.name as 's1',
	b2.name as Bookmaker,
	s2.name as 's2',
	s3.name as 's3',
	cast((s1.den / (s1.num + s1.den) * 100) + (s2.den / (s2.num + s2.den) * 100) + (s3.den / (s3.num + s3.den) * 100) as decimal(7,4)) as 'Overround'

from
	tselection s1,
	tselection s2,
	tselection s3,
	tbookmaker b1,
	tbookmaker b2,
	tevent e
where
	s1.bookmaker_id = b1.bookmaker_id and
	s2.bookmaker_id = b2.bookmaker_id and
	s3.bookmaker_id = b2.bookmaker_id and

	s1.name != s2.name and
	s1.name != s3.name and
	s2.name != s3.name and
	s3.selection_id > s2.selection_id and

	s1.event_id = e.event_id and
	s2.event_id = e.event_id and
	s3.event_id = e.event_id and

	b1.bookmaker_id != b2.bookmaker_id
having
	`Overround` < 100;